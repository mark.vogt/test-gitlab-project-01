# TEST GITLAB PROJECT 01

2018 12 16 MV: just a simple first test project created in the cloud, in order to test cloning locally for access by Visual Studio 2017 CE and VSCode 2017 as well... 

## 2018 12 16 750a

EDITED readme.md file for the first time. 

REALIZED I'll need to create ANOTHER "first" project as well - some sort PRIVATE project in order to see how that UI/UX works... 

EDITED this readme.md file from the simple web-based "Edit" editor built into the website. 

NOTICED there's a "Web IDE" which also looks intriguing, and needs to be explored.. .

ALSO realized may likely need to install some sort of gitlab-specific EXTENSIONS in VSCode?

__ EXPLORE: installing gitlab-specific extensions into VS2017CE and VSC2017 ? 

## 2018 12 16 600p MEV

FOLLOWED instructions on gitlab.com re: installation of SSH keys: 
- INSTALLED (via Chocolatey UI on Win10 laptop \\ASPEN) OpenSSH for SSH-enabling PowerShell, Git (I guess)?
- LAUNCHED (OpenSSH-enhanced) PowerShell terminal 
- GENERATED SSH key pair (public & private) using ES 2259 encryption (I think) and ddefault settings
- COPIED/PASTED public SSH key to GitLab.com account > Settings > SSH Keys 
- TESTED per instructions => WORKED | SUCCESSFUL ! 

CLONED cloud GitLab.com - hosted test repo from GitLab.com down to \\ASPEN  C:\Development\GitLab\<project name>

LAUNCHED VSCode

USED Git client in VSCode to finish cloning cloud repo to local repo 

=> SUCCESSFUL; project now local 

EDITED readme.md (these edits)

SAVED | COMMITTED locally | PUSHED to GitLab.com cloud repo... 

=> 
